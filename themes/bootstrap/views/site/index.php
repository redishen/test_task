<?php
/* @var $this SiteController */
$this->pageTitle = Yii::app()->name;
?>

<?php
$this->beginWidget('bootstrap.widgets.TbHeroUnit', array(
    'heading' => 'Welcome to ' . CHtml::encode(Yii::app()->name),
));
?>
<hr>
<?php
/*
 * При событии onclick - открывается модальное окно #myModalForm,
 * в которое ajax-ом загружается форма по url site/ajaxForm
 */
$this->widget('bootstrap.widgets.TbButton', array(
    'label' => 'Click',
    'buttonType' => 'ajaxButton',
    'url' => array('ajaxForm'),
    'ajaxOptions' => array(
        'update' => '#formData'
    ),
    'type' => 'primary',
    'htmlOptions' => array(
        'id' => 'open-modal',
        'onclick' => '$("#myModalForm").modal()',
    ),
));
?>

<?php $this->endWidget(); ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id' => 'myModalForm')); ?>

<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h4>Register</h4>
</div>

<div id="formData" class="modal-body form">

</div>

<?php $this->endWidget(); ?>