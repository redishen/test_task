<h2 class="large">Hello!</h2>
<?php
// Начало формы с ajax валидацией
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'modal-form',
    'type' => 'horizontal',
    'enableAjaxValidation' => true,
        ));
?>

<p class="note">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->textFieldRow($model, 'username'); ?>
<?php echo $form->passwordFieldRow($model, 'password'); ?>
<?php echo $form->passwordFieldRow($model, 'repeat_password'); ?>
<?php echo $form->textFieldRow($model, 'email'); ?>


<div class="modal-footer">
    <?php
    /**
     * Кнопка отправки формы через ajax POST запрос.
     * При отсутствии ошибок и возвращаемым значением [status:success] закрываем окно, сбрасываем валидацию.
     * В противном случае выводим ошибки валидации ко всем полям формы.
     */
    $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType' => 'ajaxSubmit',
        'type' => 'primary',
        'label' => 'Register',
        'url' => array('ajaxForm'),
        'ajaxOptions' => array(
            'dataType' => 'json',
            'type' => 'post',
            'success' => 'function(data) {
                    if(data.status=="success"){
                     $("#myModalForm").modal("hide");
                     $("#modal-form")[0].reset();
                    }
                     else{
                    $.each(data, function(key, val) {
                    $("#modal-form #"+key+"_em_").text(val);                                                    
                    $("#modal-form #"+key+"_em_").show();
                    });
                    }       
                }',
        ),
        'htmlOptions' => array(
            'id' => 'send-form',
        ),
    ));
    ?>
    <?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'label' => 'Close',
        'url' => '#',
        'htmlOptions' => array('data-dismiss' => 'modal'),
    ));
    ?>
</div>

<?php
// Конец формы
$this->endWidget();
?>
