<?php

class SiteController extends Controller {

    public function filters() {
        // Экшн AjaxForm может быть вызван только через ajax-запрос
        return array('ajaxOnly + ajaxForm');
    }

    /*
     *  Генерация/валидация/сохранение формы через ajax
     * 
     */
    public function actionAjaxForm() {
        $model = new User;

        // Если запрос - ajax валидация формы
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'modal-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // Если запрос - сохранение формы
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            // Возвращаем success, если сохранение/валидация не прошли - массив ошибок
            if ($model->save()) {
                echo CJSON::encode(array(
                    'status' => 'success'
                ));
                Yii::app()->end();
            } else {
                $error = CActiveForm::validate($model);
                if ($error != '[]')
                    echo $error;
                Yii::app()->end();
            }
        }
        // Рендерим представление формы
        $this->renderPartial('_form', array('model' => $model), false, true);
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        // display the login form
        $this->render('index');
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

}